package com.poc.spring.multi.processing.multiprocessing.schedulars;

import com.poc.spring.multi.processing.multiprocessing.schedulars.tasks.SaveEvenUserRunnable;
import com.poc.spring.multi.processing.multiprocessing.schedulars.tasks.SaveOddUserRunnable;
import com.poc.spring.multi.processing.multiprocessing.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

@Component
public class ScheduledTasks {
    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    @Autowired
    @Qualifier("taskExecutor")
    private Executor executor;

    @Autowired
    private UserService userService;

    @Scheduled(cron = "*/10 * * * * *")
    public void task() throws InterruptedException {
        SaveOddUserRunnable job1 = new SaveOddUserRunnable(userService);
        log.info("executing job111111111111111111111");
        executor.execute(job1);
        SaveEvenUserRunnable job2 = new SaveEvenUserRunnable(userService);
        log.info("executing job222222222222222222222");
        executor.execute(job2);
    }
}
