package com.poc.spring.multi.processing.multiprocessing.schedulars.tasks;

import com.poc.spring.multi.processing.multiprocessing.entities.User;
import com.poc.spring.multi.processing.multiprocessing.services.UserService;

import java.util.ArrayList;
import java.util.List;

public class SaveEvenUserRunnable implements Runnable {

    private  static  Integer count =0;
    public SaveEvenUserRunnable() {
    }

    public SaveEvenUserRunnable(UserService userService) {
        this.userService = userService;
    }

    private UserService userService;

    @Override
    public void run() {
        if(userService == null){
            System.out.println("userService is null");
        }else{
            count+=2;
            User oddUser = new User();
            oddUser.setName("evenUser"+count);
            oddUser.setEmail("evenUser@even"+count);
            oddUser.setGender("even"+count);
            List<User> users = new ArrayList<>();
            users.add(oddUser);
            userService.saveUsersFromSchedular(users);
        }

    }
}
