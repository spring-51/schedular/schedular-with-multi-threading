package com.poc.spring.multi.processing.multiprocessing.services;

import com.poc.spring.multi.processing.multiprocessing.entities.User;
import com.poc.spring.multi.processing.multiprocessing.repos.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class UserService {
    @Autowired
    private UserRepo userRepo;

    Logger logger = LoggerFactory.getLogger(UserService.class);


    public void saveUsersFromSchedular(List<User> users) {
        // long startTime = System.currentTimeMillis();

        logger.info("saveUsersFromSchedular: list of users of {}", users, ""+Thread.currentThread().getName());
        userRepo.saveAll(users);

        // long endTime = System.currentTimeMillis();
        // logger.info("Total time {}", (endTime-startTime));
    }

}
