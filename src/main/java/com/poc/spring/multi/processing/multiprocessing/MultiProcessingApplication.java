package com.poc.spring.multi.processing.multiprocessing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling  // This annotation ensures that a background task executor is created.
                   // Without it, nothing gets scheduled.
                   // It uses Multi threading Executor framework
public class MultiProcessingApplication {
    public static void main(String[] args) {
        SpringApplication.run(MultiProcessingApplication.class, args);
    }
}
