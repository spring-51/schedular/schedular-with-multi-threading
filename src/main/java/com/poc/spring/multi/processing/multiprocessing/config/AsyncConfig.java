package com.poc.spring.multi.processing.multiprocessing.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

@Configuration
@EnableAsync // it uses Thread pool i.e Executor Framework of java
public class AsyncConfig {

    @Bean(name = "taskExecutor")
    public Executor taskExecutor(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2); // defining thread pool core capacity /size i.e. initial thread size
        executor.setMaxPoolSize(2); // defining thread pool max allowed capacity /size
                                    // to corePoolSize we can add more thread, till it reaches maxPoolSize
                                    // how can we add thread to thread pool ?
        executor.setQueueCapacity(100); // this number of task can wait in blocking queue
        executor.setThreadNamePrefix("userThread-"); // add prefix to running thread
        return executor;
    }
}
