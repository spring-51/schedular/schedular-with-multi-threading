package com.poc.spring.multi.processing.multiprocessing.schedulars.tasks;

import com.poc.spring.multi.processing.multiprocessing.entities.User;
import com.poc.spring.multi.processing.multiprocessing.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

public class SaveOddUserRunnable implements Runnable {

    private  static  Integer count =-1;
    public SaveOddUserRunnable() {
    }

    public SaveOddUserRunnable(UserService userService) {
        this.userService = userService;
    }

    private UserService userService;

    @Override
    public void run() {
        if(userService == null){
            System.out.println("userService is null");
        }else{
            count+=2;
            User oddUser = new User();
            oddUser.setName("oddUser"+count);
            oddUser.setEmail("oddUser@odd"+count);
            oddUser.setGender("odd"+count);
            List<User> users = new ArrayList<>();
            users.add(oddUser);
            userService.saveUsersFromSchedular(users);
        }

    }
}
